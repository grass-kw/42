/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 13:28:06 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 14:43:08 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_CLASS_HPP
# define ZOMBIE_CLASS_HPP

#include <string>
#include <iostream>

class Zombie
{
public:
	Zombie(std::string name, std::string type);
	~Zombie();
	void			announce();
	// Getter and setter //
	std::string		getType(void) const;
	void			setType(std::string type);
	std::string		getName(void) const;
	void			setType(std::string name);
	// end Getter and setter //


private:
	std::string		_type;
	std::string		_name;
	Zombie();
};

std::ostream &	operator<<( std::ostream & os, Zombie const & obj);
#endif
