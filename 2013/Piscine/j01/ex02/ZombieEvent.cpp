/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 13:28:06 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 15:05:21 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <iostream>

ZombieEvent::ZombieEvent( ) :
{
	std::cout << "Parametre construtor called " << std::endl;
}

ZombieEvent::~ZombieEvent(void)
{
	std::cout << " destructor construtor called " << std::endl;
}

// setZombieType method definition here //

// Getter and setter //
std::string		*getType(void) const
{
	return &(this->_type);
}

void			setType(std::string *type)
{
	this->_type = type;
}

// end Getter and setter //
