/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 13:28:06 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 14:36:04 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

Zombie::Zombie( void )
{
	std::cout << "Parametre construtor called " << std::endl;
}

Zombie::Zombie(std::string type, std::string name) : _name(name), _type(type)
{
	std::cout << "Parametre Argument construtor called " << std::endl;
}

Zombie::~Zombie( void)
{
	std::cout << " destructor construtor called " << std::endl;
}

void			Zombie::announce()
{
	std::cout << "<" << this->getName() << " (" << this->getType() << ")> Braiiiiiiinnnssss..." << std::endl;
}

// Getter and setter //
std::string		Zombie::getType(void) const
{
	return	this->_type;
}

void		Zombie::setType(std::string type)
{
	this->_type = type;
}

std::string		Zombie::getName(void) const
{
	return	this->_name;
}

void		Zombie::setType(std::string name)
{
	this->_name = name;
}

// end Getter and setter //
