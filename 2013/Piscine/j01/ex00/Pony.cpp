/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 10:09:11 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 11:59:40 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"
#include <iostream>

Pony::Pony( )
{
	std::cout << "Parameter construtor called " << std::endl;
}

Pony::~Pony( void)
{
	std::cout << " destructor construtor called " << std::endl;
}

Pony::Pony(std::string name, std::string color, int age) : _name(name), _color(color), _age(age)
{
	std::cout << "Parameter construtor called " << std::endl;
}

/* Allocation dynamique sur le tas lors de l'execution du programme (new)*/
void	Pony::ponyOnTheHeap(std::string name, std::string color, int age)
{
	Pony *instance = new Pony(name, color, age);
	std::cout << "ponyOnTheHeap: " << std::endl;
	std::cout << "The Pony " << instance->getName() << " with a " << instance->getColor() << " of " << instance->getAge() << " old have been created" << std::endl;
}

/* Allocation statique dans la pile d'execution lors de la creation du binare (lors de la declaration des instance)*/
void	Pony::ponyOnTheStack(std::string name, std::string color, int age)
{
	Pony instance = Pony(name, color, age);
	std::cout << "ponyOnTheStack: " << std::endl;
	std::cout << "The Pony " << instance.getName() << " with a " << instance.getColor() << " of " << instance.getAge() << " old have been created" << std::endl;
}

// Getter and setter //

std::string	Pony::getName(void) const
{
	return this->_name;
}

std::string	Pony::getColor(void) const
{
	return this->_color;
}

int	Pony::getAge(void) const
{
	return this->_age;
}
// end Getter and setter //
