/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 13:28:06 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 14:53:35 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_CLASS_HPP
# define ZOMBIEEVENT_CLASS_HPP

#include <string>
#include <iostream>

class ZombieEvent
{
public:
	ZombieEvent();
	~ZombieEvent();
	void			setZombieType(std::string type);
	// Getter and setter //
	std::string		*getType(void) const;
	void			setType(std::string *type);
	// end Getter and setter //
private:
	std::string		_type[];
	ZombieEvent();

};
#endif
