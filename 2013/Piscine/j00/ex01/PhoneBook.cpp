/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 11:18:53 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/20 18:29:41 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

PhoneBook::PhoneBook()
{
	this->_nbr = 0;
	std::cout << "constructor called" << std::endl;
}

PhoneBook::~PhoneBook()
{
	std::cout << "Phonebook destructor called" << std::endl;
}

void PhoneBook::ft_add()
{
	std::string first_name;
	std::string	last_name;
	std::string	nickname;
	std::string login;
	std::string postal_add;
	std::string	email_add;
	std::string	phone_number;
	std::string	birthday;
	std::string	favorite_meal;
	std::string	underwear_color;
	std::string	darknest_secret;

	if (this->getNbr() == 7)
	{
		std::cout << "The phonebook is full you can't add contact anymore !" << std::endl;
		return ;
	}
	std::cout << "Fisrt name :" << std::endl;
	std::cin >> first_name;
	std::cout << "last_name :" << std::endl;
	std::cin >> last_name;
	std::cout << "nickname_name :" << std::endl;
	std::cin >> nickname;
	std::cout << "Login :" << std::endl;
	std::cin >> login;
	std::cout << "postal_add :" << std::endl;
	std::cin >> postal_add;
	std::cout << "email_add :" << std::endl;
	std::cin >> email_add;
	std::cout << "phone_number :" << std::endl;
	std::cin >> phone_number;
	std::cout << "birthday :" << std::endl;
	std::cin >> birthday;
	std::cout << "favorite_meal :" << std::endl;
	std::cin >> favorite_meal;
	std::cout << "underwear_color :" << std::endl;
	std::cin >> underwear_color;
	std::cout << "darknest_secret :" << std::endl;
	std::cin >> darknest_secret;

	Contact contact = Contact(first_name, last_name, nickname, login,
	 postal_add, email_add, phone_number, birthday,
	 favorite_meal, underwear_color, darknest_secret);
	this->setContact(contact);
}

void PhoneBook::ft_search()
{
	std::cout << "___________________________________________" << std::endl;
	std::cout << "     index|first name| last name| nickname" << std::endl;
	std::cout << "__________|__________|__________|__________" << std::endl;
}

void PhoneBook::ft_exit()
{
	exit(0);
}

Contact	PhoneBook::getContact(void) const
{
	return this->_contact[8];
}

void	PhoneBook::setContact(Contact contact)
{
	this->_contact[this->getNbr()] = contact;
	this->setNbr();
}

int		PhoneBook::getNbr(void) const
{
	return this->_nbr;
}

void	PhoneBook::setNbr(void)
{
	this->_nbr++;
}	