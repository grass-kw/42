/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBook.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 11:19:14 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/21 07:54:04 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_HPP
# define PHONEBOOK_HPP
# include <string>
# include <iostream>
# include <stdlib.h>
# include "Contact.hpp"

class PhoneBook
{
private:
	Contact _contact[8];
	int 	_nbr;

public:
	PhoneBook();
	~PhoneBook();
	void	ft_add();
	void	ft_search();
	void	ft_exit();
	Contact	getContact(void) const;
	void	setContact(Contact contact);
	int		getNbr(void) const;
	void	setNbr(void);
};

#endif
