/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/21 08:02:59 by anonymous         #+#    #+#             */
/*   Updated: 2015/04/30 20:52:52 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP
# include <iostream>
# include <cmath>
class Fixed
{
private:
	int _nbr;
	int const _nbrbits;
public:
	Fixed();
	Fixed(int const i);
	Fixed(float const i);
	~Fixed();
	Fixed(Fixed const & src);
	Fixed & operator=(Fixed const & src);
	int 	getRawBits( void ) const;
	void 	setRawBits( int const raw );
	float 	toFloat(void) const;
	int 	toInt(void) const;
};

std::ostream &	operator<<(std::ostream & os, Fixed const & rhs);

#endif