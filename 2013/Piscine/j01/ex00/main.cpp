/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 10:09:17 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 12:03:33 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

int	main()
{
	Pony test;
	// ponyOnTheStack Test//
	test.ponyOnTheStack("jojo", "rouge", 42);
	test.ponyOnTheStack("jean", "rouge", 42);
	test.ponyOnTheStack("simon", "rouge", 42);
	//  ponyOnTheHeap Test//
	test.ponyOnTheHeap("roger", "yellow", 7);
	test.ponyOnTheHeap("illan", "yellow", 7)
	test.ponyOnTheHeap("greg", "yellow", 7)
}
