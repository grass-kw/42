/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 13:28:21 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/19 23:45:06 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PhoneBook.hpp"

/*
*** print a prompt
*/

static void	ft_prompt()
{
	std::cout << "<Phonebook> :";
}

/*
*** verify if command is valid
*/

static void ft_check_cmd(std::string buf, PhoneBook *phonebook)
{
	// std::cout << "buf : " << buf << std::endl;
	if (buf == "EXIT" || buf == "exit")
		phonebook->ft_exit();
	else if (buf == "ADD" || buf == "add")
		phonebook->ft_add();
	else if (buf == "SEARCH" || buf == "search")
		phonebook->ft_search();
	else
		std::cout << "command not find. try again" << std::endl;
}

int main()
{
	std::string	buf;
	PhoneBook phonebook;

	while (42)
	{ 	
		ft_prompt();
		std::cin >> buf;
		ft_check_cmd(buf, &phonebook);
	}
	return (0);
}
