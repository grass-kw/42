/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 20:07:29 by anonymous         #+#    #+#             */
/*   Updated: 2015/04/19 22:08:41 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"

Contact::Contact()
{}

Contact::Contact(std::string f_n, std::string	l_n, std::string nn, std::string l,
		std::string p_a, std::string e_a, std::string p_n, std::string b,
		std::string f_m, std::string u_c, std::string d_s) : _first_name(f_n), _last_name(l_n), _nickname(nn), _login(l), _postal_add (p_a), _email_add(e_a),  _phone_number(p_n), _birthday(b), _favorite_meal(f_m), _underwear_color(u_c), _darknest_secret(d_s)
{
}

Contact::~Contact()
{}

	std::string Contact::getFirstname(void) const
	{
		return _first_name;
	}

	std::string Contact::getLastname(void) const
	{
		return _last_name;
	}

	std::string Contact::getNickname(void) const
	{
		return _nickname;
	}
	std::string Contact::getLogin(void) const
	{
		return _login;
	}
	std::string Contact::getPostaladd(void) const
	{
		return _postal_add;
	}
	std::string Contact::getEmailadd(void) const
	{
		return _email_add;
	}
	std::string Contact::getPhonenumber(void) const
	{
		return _phone_number;
	}
	std::string Contact::getBirthday(void) const
	{
		return _birthday;
	}
	std::string Contact::getFavorite_meal(void) const
	{
		return _favorite_meal;
	}
	std::string Contact::getUnderwearmeal(void) const
	{
		return _underwear_color;
	}
	std::string Contact::getDarknestsecret(void) const
	{
		return _darknest_secret;
	}