/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/21 08:03:36 by anonymous         #+#    #+#             */
/*   Updated: 2015/04/30 21:07:45 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

Fixed::Fixed() : _nbr(0), _nbrbits(8)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(int const i) : _nbr((int)i), _nbrbits(8)
{
	std::cout << "Int constructor called" << std::endl;
}

Fixed::Fixed(float const i) : _nbr((float)roundf(i)), _nbrbits(8)
{
	std::cout << "Float constructor called" << std::endl;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called" << std::endl;
}

Fixed::Fixed(Fixed const & src) : _nbrbits(8)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return ;
}

Fixed	& Fixed::operator=(Fixed const & src)
{
	std::cout << "Assignation operator called" << std::endl;
	this->_nbr = src.getRawBits();
	return (*this);
}

int		Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called" << std::endl;
	return this->_nbr;
}

void	Fixed::setRawBits(int const raw)
{
	this->_nbr = raw;
}

float 	Fixed::toFloat(void) const
{
	return roundf(this->_nbr);
}

int		Fixed::toInt(void) const
{
	return this->_nbr;
}

std::ostream &	operator<<(std::ostream & os, Fixed const & rhs)
{
	os << rhs.toFloat();
	return os;
}