/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anonymous <anonymous@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/18 20:07:11 by anonymous         #+#    #+#             */
/*   Updated: 2015/04/19 22:09:19 by anonymous        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
# define CONTACT_HPP
# include <string>
# include <iostream>
# include <stdlib.h>

class Contact
{
private:
	std::string	_first_name;
	std::string	_last_name;
	std::string	_nickname;
	std::string	_login;
	std::string	_postal_add;
	std::string	_email_add;
	std::string	_phone_number;
	std::string	_birthday;
	std::string	_favorite_meal;
	std::string	_underwear_color;
	std::string	_darknest_secret;

public:
	Contact();
	Contact(std::string f_n, std::string	l_n, std::string nn, std::string l,
		std::string p_a, std::string e_a, std::string p_n, std::string b,
		std::string f_m, std::string u_c, std::string d_s);
	~Contact();

	std::string getFirstname(void) const;
	std::string getLastname(void) const;
	std::string getNickname(void) const;
	std::string getLogin(void) const;
	std::string getPostaladd(void) const;
	std::string getEmailadd(void) const;
	std::string getPhonenumber(void) const;
	std::string getBirthday(void) const;
	std::string getFavorite_meal(void) const;
	std::string getUnderwearmeal(void) const;
	std::string getDarknestsecret(void) const;
};

#endif