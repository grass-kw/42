/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/10 00:03:40 by anonymous         #+#    #+#             */
/*   Updated: 2015/04/10 10:48:21 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

static char	ft_toupper(char c)
{
	if (c >= 'a' && c <= 'z')
		return (c - 32);
	return (c);
}

static void	ft_putline(char const *s)
{
	int	i;

	i = 0;
	while (s[i])
	{
		std::cout << ft_toupper(s[i]);
		i++;
	}
}

int main(int argc, char const *argv[])
{
	int i;

	if (argc == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	else
	{
		i = 1;
		while (argv[i])
		{
			ft_putline(argv[i]);
			i++;
		}
		std::cout << std::endl;
	}
	return (0);
}
