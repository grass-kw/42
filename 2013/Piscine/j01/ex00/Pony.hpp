/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/13 10:14:14 by grass-kw          #+#    #+#             */
/*   Updated: 2015/04/13 11:15:48 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_CLASS_HPP
# define PONY_CLASS_HPP

#include <string>
#include <iostream>

class Pony
{
private:
	std::string	_name;
	std::string	_color;
	int			_age;

public:
				Pony();
				~Pony();
				Pony(std::string name, std::string color, int age);
	std::string	getName(void) const;
	std::string	getColor(void) const;
	int			getAge(void) const;
	void		ponyOnTheHeap(std::string name, std::string color, int age);
	void		ponyOnTheStack(std::string name, std::string color, int age);
};
#endif
